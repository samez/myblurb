import { config } from 'dotenv';
import { UserService } from './api/user.service';
import { HashService } from './services/hash.service';
import { Request, Response } from 'express';
import container from './inversify.config';
import { LoginDto } from './interface';
import * as moment from 'moment';
import { sign } from 'jsonwebtoken';

const express = require('express');
const app = express();
const port = 3000;

config();
const userService = container.resolve<UserService>(UserService);
const hashService = container.resolve<HashService>(HashService);

app.get('/', (req, res) => {
	res.send('Hello World!');
});

app.post('/login', async (req: Request<LoginDto>, res: Response) => {
	const { username, password } = req.body;
	try {
		let user = await userService.find({ username });
		if (hashService.compare(password, user.password)) {
			const tokenSecret = Math.floor(
				Math.pow(10, Number(process.env.JSON_TOKEN_SECRET_NUMBER_LENGTH)) * Math.random()
			);
			user = await userService.update(user.id, { tokenSecret: tokenSecret.toString() });
			const token = sign(
				{
					iss: user.id,
					expires: moment(new Date())
						.add(5, 'days')
						.unix()
				},
				`secret${user.tokenSecret}`
			);
			return { Authorization: token };
		} else {
			return Error('Wrong username or password');
		}
	} catch (error) {
		console.log('There was an error', error);
	}
});

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
