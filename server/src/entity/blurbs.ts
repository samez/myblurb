import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Blurbs {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column('text')
	createdAt: string;

	@Column('text')
	content: string;
}
