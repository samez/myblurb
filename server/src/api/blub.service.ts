import { ConnectionService } from '../services/connection.service';
import { Blurbs } from '../entity/blurbs';
import { inject, injectable } from 'inversify';
import TYPES from '../types';
import 'reflect-metadata';

@injectable()
export class UserService {
	constructor(@inject(TYPES.ConnectionService) private connectionService: ConnectionService) {}

	async getBlurbs(pagination: number, paginationLength: number): Promise<Blurbs> {
		return new Promise(async (res, reject) => {
			try {
				const users = await this.connectionService.findBlurbs();
				if (users.length > 0) {
					res(users[0]);
				} else {
					reject(new Error('Username not found'));
				}
			} catch (error) {
				reject(error);
			}
		});
	}
}
