export default interface BlurbDto {
	id: string;
	createdAt: Date;
	content: string;
}
